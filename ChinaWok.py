# Librerias e importaciones
import random
from tabulate import tabulate

# Variables y Constantes
MASTER_CLOCK = 0
GIANT_NUMBER = 1000000
maxPersonas = 90 # Maximo de personas en el restaurante
cantMeseros = 16
cantCajeros = 5
cantAsisGer = 3
minCliente = 1
maxCliente = 5
cantClientesMesaTres = 3
minTServicioMesero = 1  # Tiempo promedio de cada acción del mesero
maxTServicioMesero = 2
minTOrdenar = 1
maxTOrdenar = 5
minTEntrada = 3
maxTEntrada = 5
minTComidaPrincipal = 15
maxTComidaPrincipal = 20
minTComer = 30
maxTComer = 45
minTComerExtra = 10
maxTComerExtra = 15
TOTAL_PERSONAS_ATENDIDAS = 0

# Arreglos
colaEspera = []  # Cola de espera -> [cantidad]
colaOrdenar = []  # Cola para ordenar -> [cantidad, tiempoEspera]
colaEsperaEntrada = []  # Cola para esperar la entrada -> [cantidad, tiempoEspera]
# Cola para el platillo principal -> [cantidad, tiempoEspera]
colaEsperaComidaPrincipal = []
colaExtra = []  # Cola para un pedido extra -> [cantidad, tiempoEspera]
colaEsperaExtra = []  # Cola de espera para el pedido extra -> [cantidad, tiempoEspera]
colaPago = []  # Cola para pagar -> [cantidad, tiempoEspera]
meseros = []  # Mesero -> [disponible, tiempoServicio]
cajeros = []  # Cajero -> [disponible, tiempoServicio]
# SE CUENTA CON 12 MESAS DE 5 PERSONAS Y 10 DE 3 PERSONAS
mesasCinco = []  # Mesa de Cinco -> [capacidad, disponible]
mesasTres = []  # Mesa de Tres -> [capacidad, disponible]

# crearMesas(): Crea los objetos de las mesas, tanto de 3 como 5 personas, que posee el restaurante
def crearMesas():
    for mesa in range(0, 60, 5):
        mesasCinco.append({'capacidad': 5, 'disponible': True})
    for mesa in range(0, 30, 3):
        mesasTres.append({'capacidad': 3, 'disponible': True})

# crearMeseros(): Crea los objetos de los meseros que atenderan en el restaurante
# disponible: Si el mesero esta disponible o no
# tiempoServicio: Unidad de tiempo en que volvera a estar disponible
def crearMeseros():
    for mesero in range(0, cantMeseros):
        meseros.append({'disponible': True, 'tiempoServicio': random.randint(
            minTServicioMesero, maxTServicioMesero)})

# crearCajeros(): Crea los objetos de los cajeros que atenderan en el restaurante
# disponible: Si el cajero esta disponible o no
# tiempoServicio: Unidad de tiempo en que volvera a estar disponible
def crearCajeros():
    for cajero in range(0, cantCajeros):
        cajeros.append({'disponible': True, 'tiempoServicio': random.randint(
            minTServicioMesero, maxTServicioMesero)})

'''
ubicarMesaDisponible(): Verifica en que tipo de mesa se sentara el grupo de personas y lo ubica en una de ellas
dejando la mesa como no disponible. Si no hay mesas disponibles para el grupo de personas se agregara a la cola de 
espera fuera del restaurante
'''
def ubicarMesaDisponible(cantCliente, masterclock):
    encontroMesa = False
    if (cantCliente[0] > cantClientesMesaTres):
        for mesa in mesasCinco:
            if(mesa['disponible']):
                mesa['disponible'] = False
                encontroMesa = True
                colaOrdenar.append({'cantidad': cantCliente[0], 'tiempoEspera': (random.randint(
                    minTOrdenar, maxTOrdenar) + masterclock)})
                break
    else:
        for mesa in mesasTres:
            if(mesa['disponible']):
                mesa['disponible'] = False
                encontroMesa = True
                colaOrdenar.append({'cantidad': cantCliente[0], 'tiempoEspera': (random.randint(
                    minTOrdenar, maxTOrdenar) + masterclock)})
                break
        # Si no se encontró una mesa de 3 para los grupos pequeños se verifican si hay mesas de 5 para ubicarlos
        if(not(encontroMesa)):
            for mesa in mesasCinco:
                if(mesa['disponible']):
                    mesa['disponible'] = False
                    encontroMesa = True
                    colaOrdenar.append({'cantidad': cantCliente[0], 'tiempoEspera': (random.randint(
                        minTOrdenar, maxTOrdenar) + masterclock)})
                    break
    if(not(encontroMesa) and not(cantCliente in colaEspera)):
        colaEspera.append(cantCliente)
    return encontroMesa

'''
verificarMeseroDisponible(): Verifica si hay un mesero que puede realizar la accion, de ser asi, pasara
a no estar disponible, ademas se le asigna el tiempo en que volvera a estarlo.
'''
def verificarMeseroDisponible():
    global MASTER_CLOCK
    for mesero in meseros:
        if(mesero['disponible']):
            mesero['disponible'] = False
            mesero['tiempoServicio'] = MASTER_CLOCK + \
                random.randint(minTOrdenar, maxTOrdenar)
            return True
    return False

'''
verificarCajeroDisponible(): Verifica si hay un cajero que puede realizar la accion, de ser asi, pasara
a no estar disponible, ademas se le asigna el tiempo en que volvera a estarlo.
'''
def verificarCajeroDisponible():
    global MASTER_CLOCK
    for cajero in cajeros:
        if(cajero['disponible']):
            cajero['disponible'] = False
            cajero['tiempoServicio'] = MASTER_CLOCK + \
                random.randint(minTOrdenar, maxTOrdenar)
            return True
    return False

'''
moverClienteSigCola(): Metodo general, que mueve al grupo de personas de la cola en que esta, a la cola siguiente.
Ademas se asigna el tiempo en que permanecera en la siguiente cola.
'''
def moverClienteSigCola(minT, maxT, colaAnterior, colaSiguiente, clientes):
    global MASTER_CLOCK
    if(verificarMeseroDisponible()):
        colaAnterior.remove(clientes)
        clientes['tiempoEspera'] = random.randint(minT, maxT) + MASTER_CLOCK
        colaSiguiente.append(clientes)
    else:
        clientes['tiempoEspera'] = random.randint(minT, maxT) + GIANT_NUMBER

'''
ordenarComida(): Simula cuando el grupo de personas ordena, pasandolos a la siguiente cola que seria
esperar su platillo de entrada.
'''
def ordenarComida(clientes):
    moverClienteSigCola(minTEntrada, maxTEntrada,
                        colaOrdenar, colaEsperaEntrada, clientes)

'''
recibirEntrada(): Simula cuando el grupo de personas recibe su platillo de entrada, pasandolos a la siguiente cola que seria
esperar su platillo principal.
'''
def recibirEntrada(clientes):
    moverClienteSigCola(minTComidaPrincipal, maxTComidaPrincipal,
                        colaEsperaEntrada, colaEsperaComidaPrincipal, clientes)

'''
recibirComida(): Simula cuando el grupo de personas recibe su platillo principal, aqui se define si ellos pediran algun extra
o iran directamente a pagar cuando terminen su comida, moviendolos a la cola correspondiente.
'''
def recibirComida(clientes):
    if(random.random() > 0.5):
        moverClienteSigCola(minTComer, maxTComer,
                            colaEsperaComidaPrincipal, colaExtra, clientes)
    else:
        moverClienteSigCola(minTComer, maxTComer,
                            colaEsperaComidaPrincipal, colaPago, clientes)

'''
ordenarExtra(): Simula cuando el grupo de personas ha terminado de comer y procede a ordenar un platillo extra,
moviendolos a la cola de espera por su platillo extra.
'''
def ordenarExtra(clientes):
    moverClienteSigCola(minTEntrada, maxTEntrada,
                        colaExtra, colaEsperaExtra, clientes)

'''
recibirExtra(): Simula cuando el grupo de personas recibe su platillo extra, y define el tiempo en que
procederan a pagar luego de haber terminado.
'''
def recibirExtra(clientes):
    moverClienteSigCola(minTComerExtra, maxTComerExtra,
                        colaEsperaExtra, colaPago, clientes)

'''
pagarOrden(): Simula el pago del servicio, aqui se retira al grupo de personas de la cola, dejando finalmente su mesa
como disponible, ademas de llevar un conteo de cuantos grupos de personas se han atentido.
'''
def pagarOrden(clientes):
    global TOTAL_PERSONAS_ATENDIDAS
    if(verificarCajeroDisponible()):
        colaPago.remove(clientes)
        TOTAL_PERSONAS_ATENDIDAS += 1
        if (clientes['cantidad'] > cantClientesMesaTres):
            for mesa in mesasCinco:
                if(not(mesa['disponible'])):
                    mesa['disponible'] = True
                    break
        else:
            for mesa in mesasTres:
                if(not(mesa['disponible'])):
                    mesa['disponible'] = True
                    break

'''
obtenerMinimo(): Obtiene el tiempo minimo de todas las colas, para asi el MC pueda pasar a esa unidad de tiempo.
'''
def obtenerMinimo():
    global colaOrdenar, colaEsperaEntrada, colaEsperaComidaPrincipal, colaEsperaExtra, colaExtra, colaPago
    #Para los meseros y los cajeros, solo se verifican aquellos que esten ocupados
    minMeseros = 0
    meserosOcupados = []
    for mesero in meseros:
        if(not(mesero['disponible'])):
            meserosOcupados.append(mesero)

    minCajeros = 0
    cajerosOcupados = []
    for cajero in cajeros:
        if(not(cajero['disponible'])):
            cajerosOcupados.append(cajero)

    minMeseros = min(meserosOcupados, key=lambda d: d['tiempoServicio']).get(
        'tiempoServicio') if (len(meserosOcupados) != 0) else GIANT_NUMBER

    minCajeros = min(cajerosOcupados, key=lambda d: d['tiempoServicio']).get(
        'tiempoServicio') if (len(cajerosOcupados) != 0) else GIANT_NUMBER

    colaOrdenar = sorted(colaOrdenar, key=lambda k: k['tiempoEspera'])
    minOrdenar = colaOrdenar[0]['tiempoEspera'] if (
        len(colaOrdenar) != 0) else GIANT_NUMBER

    '''
    Se ordenan cada una de las colas por su tiempoEspera, esto se hace debido a que un grupo podría haber llegado antes,
    sin embargo si algun grupo siguiente, su tiempo de espera es menor, se debe atender primero, pues actuaron mas rapido en
    comparacion a el o los grupos anteriores.
    '''

    colaEsperaEntrada = sorted(
        colaEsperaEntrada, key=lambda k: k['tiempoEspera'])
    minEntrada = colaEsperaEntrada[0]['tiempoEspera'] if (
        len(colaEsperaEntrada) != 0) else GIANT_NUMBER

    colaEsperaComidaPrincipal = sorted(
        colaEsperaComidaPrincipal, key=lambda k: k['tiempoEspera'])
    minComidaPrincipal = colaEsperaComidaPrincipal[0]['tiempoEspera'] if (
        len(colaEsperaComidaPrincipal) != 0) else GIANT_NUMBER

    colaEsperaExtra = sorted(colaEsperaExtra, key=lambda k: k['tiempoEspera'])
    minEsperaExtra = colaEsperaExtra[0]['tiempoEspera'] if (
        len(colaEsperaExtra) != 0) else GIANT_NUMBER

    colaExtra = sorted(colaExtra, key=lambda k: k['tiempoEspera'])
    minExtra = colaExtra[0]['tiempoEspera'] if (
        len(colaExtra) != 0) else GIANT_NUMBER

    colaPago = sorted(colaPago, key=lambda k: k['tiempoEspera'])
    minPago = colaPago[0]['tiempoEspera'] if (
        len(colaPago) != 0) else GIANT_NUMBER

    tiempos = [minMeseros, minCajeros, minOrdenar, minEntrada,
               minComidaPrincipal, minEsperaExtra, minExtra, minPago]

    return min(tiempos)

'''
sumarTiempoEsperaMasterClock(): Suma el MC al tiempo de espera generado aleatoriamente de la accion, para definir el tiempo
en que se finalizara la accion.
'''
def sumarTiempoEsperaMasterClock(cola, masterclock):
    if (len(cola) > 0):
        cola[0]['tiempoEspera'] += masterclock

'''
coincideConMasterClock(): Verifica si el tiempo del siguiente en la cola es igual a MC, significando
que es el momento en que finaliza esa accion.
'''
def coincideConMasterClock(cola, masterclock):
    if(len(cola) > 0):
        if(cola[0]['tiempoEspera'] == masterclock):
            return True
        else:
            return False
    else:
        return False

'''
coincideServicioConMasterClock(): Verifica si el tiempoServicio del siguiente en la cola es igual a MC, significando
que es el momento en que finaliza ese servicio.
'''
def coincideServicioConMasterClock(cola, masterclock):
    for trabajador in cola:
        if(trabajador['tiempoServicio'] == masterclock):
            trabajador['disponible'] = True
    liberarClienteBloqueado(masterclock)

'''
llegaNuevoCliente(): Define aleatoriamente si en el paso actual llegara un grupo nuevo o no.
'''
def llegaNuevoCliente():
    if(random.random() > 0.5):
        return True
    else:
        return False

'''
liberarClienteBloqueado(): Libera un grupo de personas BLOQUEADO de la cola, significando que ya hay trabajadores que puedan atenderlo.
BLOQUEADO = Llego el momento de su accion, mas no habia trabajadores disponibles.
'''
def liberarClienteBloqueado(masterclock):
    if(len(colaOrdenar) > 0):
        if(colaOrdenar[0]['tiempoEspera'] > GIANT_NUMBER):
            colaOrdenar[0]['tiempoEspera'] = masterclock + 1

    if(len(colaEsperaEntrada) > 0):
        if(colaEsperaEntrada[0]['tiempoEspera'] > GIANT_NUMBER):
            colaEsperaEntrada[0]['tiempoEspera'] = masterclock + 1

    if(len(colaEsperaComidaPrincipal) > 0):
        if(colaEsperaComidaPrincipal[0]['tiempoEspera'] > GIANT_NUMBER):
            colaEsperaComidaPrincipal[0]['tiempoEspera'] = masterclock + 1

    if(len(colaEsperaExtra) > 0):
        if(colaEsperaExtra[0]['tiempoEspera'] > GIANT_NUMBER):
            colaEsperaExtra[0]['tiempoEspera'] = masterclock + 1

    if(len(colaExtra) > 0):
        if(colaExtra[0]['tiempoEspera'] > GIANT_NUMBER):
            colaExtra[0]['tiempoEspera'] = masterclock + 1

    if(len(colaPago) > 0):
        if(colaPago[0]['tiempoEspera'] > GIANT_NUMBER):
            colaPago[0]['tiempoEspera'] = masterclock + 1

'''
avanzarEtapa(): Mueve al grupo de personas a la siguiente etapa, esto solo si coincide su tiempoEspera con el MC,
lo que significa que su accion ha finalizado.
'''
def avanzarEtapa(masterclock, cola, siguienteEtapa):
    if(coincideConMasterClock(cola, masterclock)):
        sumarTiempoEsperaMasterClock(cola, masterclock)
        siguienteEtapa(cola[0])


IMPRESION_GLOBAL = [] # Salida a imprimir de cada paso

'''
imprimirInformacionPaso(): Crea la salida con la informacion del paso actual, la cual incluye:
Paso Actual, MC, Tiempo Siguiente Cola Ordenar, Tiempo Siguiente Cola Espera Entrada,
Tiempo Siguiente Cola Espera Comida Principal, Tiempo Siguiente Cola Extra, Tiempo Siguiente Cola Espera Extra,
Meseros Ocupados, Meseros Disponibles, Tiempo Siguiente Cola Pago, Cajeros Ocupados, Cajeros Disponibles.
'''
def imprimirInformacionPaso(pasoActual, masterclock):
    IMPRESION = []
    IMPRESION.append(pasoActual)
    IMPRESION.append("  |  ")
    IMPRESION.append(masterclock)
    IMPRESION.append("  |  ")
    IMPRESION.append(colaOrdenar[0]['tiempoEspera'] if (
        len(colaOrdenar) > 0) else "C.V.")
    IMPRESION.append("  |  ")
    IMPRESION.append(len(colaOrdenar))
    IMPRESION.append("  |  ")
    IMPRESION.append(colaEsperaEntrada[0]['tiempoEspera'] if (
        len(colaEsperaEntrada) > 0) else "C.V.")
    IMPRESION.append("  |  ")
    IMPRESION.append(len(colaEsperaEntrada))
    IMPRESION.append("  |  ")
    IMPRESION.append(colaEsperaComidaPrincipal[0]['tiempoEspera'] if (
        len(colaEsperaComidaPrincipal) > 0) else "C.V.")
    IMPRESION.append("  |  ")
    IMPRESION.append(len(colaEsperaComidaPrincipal))
    IMPRESION.append("  |  ")
    IMPRESION.append(colaExtra[0]['tiempoEspera'] if (
        len(colaExtra) > 0) else "C.V.")
    IMPRESION.append("  |  ")
    IMPRESION.append(len(colaExtra))
    IMPRESION.append("  |  ")
    IMPRESION.append(colaEsperaExtra[0]['tiempoEspera'] if (
        len(colaEsperaExtra) > 0) else "C.V.")
    IMPRESION.append("  |  ")
    IMPRESION.append(len(colaEsperaExtra))
    IMPRESION.append("  |  ")
    meserosOcupados = 0
    meserosDisponibles = 0
    for mesero in meseros:
        if(not(mesero['disponible'])):
            meserosOcupados += 1
        else:
            meserosDisponibles += 1
    IMPRESION.append(meserosOcupados)
    IMPRESION.append("  |  ")
    IMPRESION.append(meserosDisponibles)
    IMPRESION.append("  |  ")
    IMPRESION.append(colaPago[0]['tiempoEspera'] if (
        len(colaPago) > 0) else "C.V.")
    IMPRESION.append("  |  ")
    IMPRESION.append(len(colaPago))
    IMPRESION.append("  |  ")
    cajerosOcupados = 0
    cajerosDisponibles = 0
    for cajero in cajeros:
        if(not(cajero['disponible'])):
            cajerosOcupados += 1
        else:
            cajerosDisponibles += 1
    IMPRESION.append(cajerosOcupados)
    IMPRESION.append("  |  ")
    IMPRESION.append(cajerosDisponibles)
    IMPRESION_GLOBAL.append(IMPRESION)

RESUMEN = [] # Salida con la informacion del estado final de la simulacion

'''
imprimirResumen(): Crea la salida con la informacion del estado final de la simulacion, que incluye:
Grupos en Cola Espera, Grupos en el Restaurante, Mesas 5P Disponibles, Mesas 5P Ocupadas,
Mesas 3P Disponibles, Mesas 3P Ocupadas y el Total de Grupos Atendidos.
'''
def imprimirResumen():
    RESUMEN_SIMULACION = []
    RESUMEN_SIMULACION.append(len(colaEspera))
    RESUMEN_SIMULACION.append("  |  ")
    RESUMEN_SIMULACION.append(
        len(colaOrdenar) +
        len(colaEsperaEntrada) +
        len(colaEsperaComidaPrincipal) +
        len(colaEsperaExtra) +
        len(colaExtra)
    )
    RESUMEN_SIMULACION.append("  |  ")
    mesasCincoDisponibles = 0
    mesasCincoOcupadas = 0
    mesasTresDisponibles = 0
    mesasTresOcupadas = 0
    for mesa in mesasCinco:
        if(mesa['disponible']):
            mesasCincoDisponibles += 1
        else:
            mesasCincoOcupadas += 1
    for mesa in mesasTres:
        if(mesa['disponible']):
            mesasTresDisponibles += 1
        else:
            mesasTresOcupadas += 1

    RESUMEN_SIMULACION.append(mesasCincoDisponibles)
    RESUMEN_SIMULACION.append("  |  ")
    RESUMEN_SIMULACION.append(mesasCincoOcupadas)
    RESUMEN_SIMULACION.append("  |  ")
    RESUMEN_SIMULACION.append(mesasTresDisponibles)
    RESUMEN_SIMULACION.append("  |  ")
    RESUMEN_SIMULACION.append(mesasTresOcupadas)
    RESUMEN_SIMULACION.append("  |  ")
    RESUMEN_SIMULACION.append(TOTAL_PERSONAS_ATENDIDAS)
    RESUMEN.append(RESUMEN_SIMULACION)
    return RESUMEN

'''
paso(): Simulacion de la logica de cada paso de la simulacion, que incluye:
- Mover grupos de cola espera a el restaurante
- Llegada de nuevos grupos
- Cambio en el MC
- Avanzar al siguiente de cada cola
'''
def paso(pasoactual):
    global MASTER_CLOCK
    #Verificar si hay grupos en espera, para ver si pueden ser ingresados o no al restaurante
    if(len(colaEspera) > 0):
        esperaARemover = None
        # Para quitar grupos de la cola de espera, si se verifica que hay mesas para ubicarlos
        for espera in colaEspera:
            if(ubicarMesaDisponible(espera, MASTER_CLOCK)):
                esperaARemover = espera
                break
        if(not(esperaARemover == None)):
            colaEspera.remove(esperaARemover)
    # Saber si llega un nuevo cliente, si es posible se ingresa al restaurante y si no se agrega a la cola de espera
    if(llegaNuevoCliente()):
        ubicarMesaDisponible(
            [random.randint(minCliente, maxCliente)], MASTER_CLOCK)

    minimo = obtenerMinimo()
    MASTER_CLOCK = minimo if (minimo != GIANT_NUMBER) else (MASTER_CLOCK + 1)
    imprimirInformacionPaso(pasoactual, MASTER_CLOCK)
    # CAMBIAR ESTADO DE TRABAJADORES
    coincideServicioConMasterClock(cajeros, MASTER_CLOCK)
    coincideServicioConMasterClock(meseros, MASTER_CLOCK)
    # ETAPA 1 - COLA ORDENAR
    avanzarEtapa(MASTER_CLOCK, colaOrdenar, ordenarComida)
    # ETAPA 2 - COLA ESPERA ENTRADA
    avanzarEtapa(MASTER_CLOCK, colaEsperaEntrada, recibirEntrada)
    # ETAPA 3 - COLA RECIBIR ENTRADA
    avanzarEtapa(MASTER_CLOCK, colaEsperaComidaPrincipal, recibirComida)
    # ETAPA 4 - COLA RECIBIR COMIDA PRINCIPAL Y ORDENAR EXTRA
    avanzarEtapa(MASTER_CLOCK, colaExtra, ordenarExtra)
    # ETAPA 5 - COLA RECIBIR EXTRA
    avanzarEtapa(MASTER_CLOCK, colaEsperaExtra, recibirExtra)
    # ETAPA 6 - COLA DE PAGO
    avanzarEtapa(MASTER_CLOCK, colaPago, pagarOrden)

# Pasos iniciales - Crear todas las entidades del restaurante
crearMesas()
crearMeseros()
crearCajeros()

print("""\
   __  __      _                      _     __          __       __        ________   _____       __                __                       __________  _________________
  / / / /___  (_)   _____  __________(_)___/ /___ _____/ /  ____/ /__     / ____/ /  / ___/____ _/ /   ______ _____/ /___  _____            /_  __/ __ \/ ___<  <  / ____/
 / / / / __ \/ / | / / _ \/ ___/ ___/ / __  / __ `/ __  /  / __  / _ \   / __/ / /   \__ \/ __ `/ / | / / __ `/ __  / __ \/ ___/  ______     / / / / / /\__ \/ // /___ \  
/ /_/ / / / / /| |/ /  __/ /  (__  ) / /_/ / /_/ / /_/ /  / /_/ /  __/  / /___/ /   ___/ / /_/ / /| |/ / /_/ / /_/ / /_/ / /     /_____/    / / / /_/ /___/ / // /___/ /  
\____/_/ /_/_/ |___/\___/_/  /____/_/\__,_/\__,_/\__,_/   \__,_/\___/  /_____/_/   /____/\__,_/_/ |___/\__,_/\__,_/\____/_/                /_/ /_____//____/_//_/_____/                                                                                                                                                                                                                                                                                                                                      
  ____  _                           _     _                     _             _                 _            _                   _        
 | __ )(_) ___ _ ____   _____ _ __ (_) __| | ___  ___    __ _  | | __ _   ___(_)_ __ ___  _   _| | __ _  ___(_) ___  _ __     __| | ___ _ 
 |  _ \| |/ _ \ '_ \ \ / / _ \ '_ \| |/ _` |/ _ \/ __|  / _` | | |/ _` | / __| | '_ ` _ \| | | | |/ _` |/ __| |/ _ \| '_ \   / _` |/ _ (_)
 | |_) | |  __/ | | \ V /  __/ | | | | (_| | (_) \__ \ | (_| | | | (_| | \__ \ | | | | | | |_| | | (_| | (__| | (_) | | | | | (_| |  __/_ 
 |____/|_|\___|_| |_|\_/ \___|_| |_|_|\__,_|\___/|___/  \__,_| |_|\__,_| |___/_|_| |_| |_|\__,_|_|\__,_|\___|_|\___/|_| |_|  \__,_|\___(_)
                                                                                                                                                                                                                                                                        
 .----------------. .----------------. .----------------. .-----------------..----------------.   .----------------. .----------------. .----------------. 
| .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | | .--------------. | .--------------. | .--------------. |
| |     ______   | | |  ____  ____  | | |     _____    | | | ____  _____  | | |      __      | | | | _____  _____ | | |     ____     | | |  ___  ____   | |
| |   .' ___  |  | | | |_   ||   _| | | |    |_   _|   | | ||_   \|_   _| | | |     /  \     | | | ||_   _||_   _|| | |   .'    `.   | | | |_  ||_  _|  | |
| |  / .'   \_|  | | |   | |__| |   | | |      | |     | | |  |   \ | |   | | |    / /\ \    | | | |  | | /\ | |  | | |  /  .--.  \  | | |   | |_/ /    | |
| |  | |         | | |   |  __  |   | | |      | |     | | |  | |\ \| |   | | |   / ____ \   | | | |  | |/  \| |  | | |  | |    | |  | | |   |  __'.    | |
| |  \ `.___.'\  | | |  _| |  | |_  | | |     _| |_    | | | _| |_\   |_  | | | _/ /    \ \_ | | | |  |   /\   |  | | |  \  `--'  /  | | |  _| |  \ \_  | |
| |   `._____.'  | | | |____||____| | | |    |_____|   | | ||_____|\____| | | ||____|  |____|| | | |  |__/  \__|  | | |   `.____.'   | | | |____||____| | |
| |              | | |              | | |              | | |              | | |              | | | |              | | |              | | |              | |
| '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | | '--------------' | '--------------' | '--------------' |
 '----------------' '----------------' '----------------' '----------------' '----------------'   '----------------' '----------------' '----------------'                                                                                                                                                                                                         

Esta simulacion se basa, en una de las sucursales de la cadena China Wok, ubicada en el Boulevard de Los Heroes. Tomando en cuenta un flujo simplificado de
la atencion de los clientes (Llegada, ordenar comida, recibir entradas, plato principal, optar por un postre y el pago del servicio) generando tiempos
aleatorios entre etapas de la simulacion y a su vez generando un flujo aleatorio de clientes. Como simplificacion se establece un rango fijo para el tiempo
de servicio de los trabajadores y se permite al usuario de la simulacion elegir el numero de repeticiones de la simulacion.

_ _  _ ____ ____ ____ ____ ____    ____ _       _  _ _  _ _  _ ____ ____ ____    ___  ____    ___  ____ ____ ____ ____    ____    ____ _ _  _ _  _ _    ____ ____  
| |\ | | __ |__/ |___ [__  |___    |___ |       |\ | |  | |\/| |___ |__/ |  |    |  \ |___    |__] |__| [__  |  | [__     |__|    [__  | |\/| |  | |    |__| |__/ .
| | \| |__] |  \ |___ ___] |___    |___ |___    | \| |__| |  | |___ |  \ |__|    |__/ |___    |    |  | ___] |__| ___]    |  |    ___] | |  | |__| |___ |  | |  \ .
    """)

# Pedir cantidad de pasos para la simulación
cantidadPasos = 0

while cantidadPasos <= 0:
    try:
        cantidadPasos = int(input())
        if (cantidadPasos <= 0):
            print("Ingrese un número de pasos mayor a 0")    
    except:
        print("Ingrese un número de pasos correcto")

# Ejecucion de los pasos solicitados
for x in range(0, cantidadPasos):
    paso(x)

# Creacion de la tabla de leyendas para la tabla de la simulacion
leyenda = []
leyenda1 = []
leyenda2 = []
leyenda3 = []

leyenda1.append("P: Paso")
leyenda1.append("   |   ")
leyenda1.append("MC: Master Clock")
leyenda1.append("   |   ")
leyenda1.append("C.V: Cola Vacia")
leyenda1.append("   |   ")
leyenda1.append("# X: Numero de personas en la cola X")
leyenda1.append("   |   ")

leyenda2.append("C.O.: Cola Ordenar")
leyenda2.append("   |   ")
leyenda2.append("C.E.: Cola Espera Entrada")
leyenda2.append("   |   ")
leyenda2.append("C.C.P.: Cola Comida Principal")
leyenda2.append("   |   ")
leyenda2.append("C.O.E.: Cola Ordenar Extra")
leyenda2.append("   |   ")
leyenda2.append("C.Ex.: Cola Extra")
leyenda2.append("   |   ")

leyenda3.append("M.O.: Meseros Ocupados")
leyenda3.append("   |   ")
leyenda3.append("C.P.: Cola Pago")
leyenda3.append("   |   ")
leyenda3.append("Caj. O: Cajeros Ocupados")
leyenda3.append("   |   ")
leyenda3.append("Caj. D: Cajeros Disponibles")
leyenda3.append("   |   ")

leyenda.append(leyenda1)
leyenda.append(leyenda2)
leyenda.append(leyenda3)

print("\n-----------------------------------------------------------------------------------------------------\n")
print("LEYENDA:")
print(tabulate(leyenda))
print("\n")
# Impresion de los pasos de la simulacion
print(tabulate(IMPRESION_GLOBAL, headers=["P", "", "MC", "", "C.O.", "", "# C.0.", "", "C.E.", "", "# C.E.",
      "", "C.C.P", "", "# C.C.P.", "", "C.O.E.", "", "# C.O.E.", "", "C.Ex.", "", "# C.Ex.",  "", "M.O.", "", "M.D.", "", "C.P.", "", "# C.P.", "", "Caj. O", "", "Caj. D"]))
print("\n-----------------------------------------------------------------------------------------------------\n")
# Impresion del resumen de la simulacion
print("RESUMEN - ESTADO FINAL DE LA SIMULACION\n")
print(tabulate(imprimirResumen(), headers=["P. en Cola Espera", "",
"P. en restaurante", "",
"Mesas 5P Disponibles", "",
"Mesas 5P Ocupadas", "",
"Mesas 3P Disponibles", "",
"Mesas 3P Ocupadas", "",
"P. Atendidas"]))
print("\nNOTA: EL TOTAL DE CADA COLA SE REFIERE A UN GRUPO DE PERSONAS ENTRE 1 A 5")
print("\n-----------------------------------------------------------------------------------------------------\n")
print("\nCantidad de personas en cada grupo en la cola de espera:\n")
print(tabulate(colaEspera, headers=["Cantidad de personas"]))
print("\n-----------------------------------------------------------------------------------------------------\n")